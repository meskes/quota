# Vietnamese translation for quota.
# Copyright © 2005 Free Software Foundation, Inc.
# Phan Vinh Thinh <teppi@vnoss.org>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: quota_3.12-6-vi\n"
"Report-Msgid-Bugs-To: quota@packages.debian.org\n"
"POT-Creation-Date: 2008-06-27 12:24+0200\n"
"PO-Revision-Date: 2005-05-21 09:42+0400\n"
"Last-Translator: Phan Vinh Thinh <teppi@vnoss.org>\n"
"Language-Team: Vietnamese <gnomevi-list@lists.sourceforge.net>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#. Type: boolean
#. Description
#: ../templates:1001
#, fuzzy
msgid "Send daily reminders to users over quota?"
msgstr "Hàng ngày gửi thông báo về vượt giới hạn không gian đĩa tới người dùng"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Enable this option if you want the warnquota utility to be run daily to "
"alert users when they are over quota."
msgstr ""
"Chọn dùng tùy chọn này nếu người dùng muốn tiện tích warnquota chạy hàng "
"ngày để báo cho người dùng biết nếu vượt quá giới hạn dùng đĩa."

#. Type: string
#. Description
#: ../templates:2001
#, fuzzy
msgid "Phone support number of the admin:"
msgstr "Số điện thoại hỗ trợ của nhà quản trị"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"Enter the phone number a user can call if he needs assistance with his "
"\"over quota\" emails. You do not have to enter anything here if you specify "
"a signature later."
msgstr ""
"Nhập số điện thoại để người dùng có thể gọi nếu cần với các thư «vượt quá "
"giới hạn». Không cần nhập gì ở đây nếu chỉ ra một chữ ký ở sau này."

#. Type: string
#. Description
#: ../templates:3001
#, fuzzy
msgid "Support email of the admin:"
msgstr "Địa chỉ thư hỗ trợ của nhà quản trị"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Enter the email address a user can write to if he needs assistance with his "
"\"over quota\" emails. You do not have to enter anything here if you specify "
"a signature later."
msgstr ""
"Nhập địa chỉ thư để người dùng có thể viết nếu cần với các thư «vượt quá "
"giới hạn». Không cần nhập gì ở đây nếu chỉ ra một chữ ký ở sau này."

#. Type: string
#. Description
#: ../templates:4001
#, fuzzy
msgid "From header of warnquota emails:"
msgstr "Dòng đầu thư «Người gửi» của thư cảnh báo warnquota"

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"The email address you specify here is used as the \"From:\" field of any "
"mail sent by the warnquota utility."
msgstr ""
"Địa chỉ thư ở đây dùng làm «Người gửi:» (From) cho bất kỳ thư nào gửi bởi "
"tiện ích warnquota."

#. Type: string
#. Description
#: ../templates:5001
#, fuzzy
msgid "Message of warnquota emails:"
msgstr "Nội dung của thư cảnh báo warnquota"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"The text you specify here is used as message in any mail sent by the "
"warnquota utility. Use \"|\" to specify a line break. Leave empty if you "
"want the default message."
msgstr ""
"Văn bản nhập ở đây được dùng làm nội dung của thư để tiện ích warnquota gửi "
"đi. Dùng «|» (ký tự ống dẫn) để chỉ ra một ngắt dòng. Để trống nếu muốn dùng "
"thư mặc định."

#. Type: string
#. Description
#: ../templates:6001
#, fuzzy
msgid "Signature of warnquota emails:"
msgstr "Chữ ký của email cảnh báo warnquota"

#. Type: string
#. Description
#: ../templates:6001
msgid ""
"The text you specify here is used as signature in any mail sent by the "
"warnquota utility. Use \"|\" to specify a line break. Leave empty if you "
"want the default signature."
msgstr ""
"Văn bản nhập ở đây được dùng làm chữ ký của bất kỳ thư nào do tiệních "
"warnquota gửi đi. Dùng «|» (ký tự ống dẫn) để chỉ ra một ngắt dòng. Để trống "
"nếu muốn dùng thư mặc định."

#. Type: note
#. Description
#: ../templates:7001
msgid "rpc.rquota behaviour changed"
msgstr "Đã thay đổi đặc điểm của rpc.rquota"

#. Type: note
#. Description
#: ../templates:7001
msgid ""
"The behaviour of rpc.rquotad changed. To be able to set quota rpc.rquotad "
"has to be started with option '-S'."
msgstr ""
"Đã thay đổi đặc điểm của rpc.rquota rồi. Để có thể đặt giới hạn không gian "
"đĩa, phải chạy rpc.rquotad với tùy chọn '-S'."

#. Type: string
#. Description
#: ../templates:8001
#, fuzzy
msgid "Message of warnquota group emails:"
msgstr "Nội dung của thư cho nhóm của warnquota"

#. Type: string
#. Description
#: ../templates:8001
msgid ""
"The text you specify here is used as message in any mail sent by the "
"warnquota utility for groups that are over quota. Use \"|\" to specify a "
"line break. Leave empty if you want the default message."
msgstr ""
"Văn bản nhập ở đây được dùng làm nội dung của thư để tiện ích warnquota gửi "
"tới nhóm người dùng vượt quá giới hạn không gian đĩa. Dùng «|» (ký tự ống "
"dẫn) để chỉ ra một ngắt dòng. Để trống nếu muốn dùng thư mặc định."

#. Type: string
#. Description
#: ../templates:9001
#, fuzzy
msgid "Subject header of warnquota emails:"
msgstr "Dòng đầu Chủ đề của thư cảnh báo warnquota"

#. Type: string
#. Description
#: ../templates:9001
msgid ""
"The text you specify here is used as the \"Subject:\" field of any mail sent "
"by the warnquota utility."
msgstr ""
"Văn bản đưa ra ở đây dùng làm «Chủ đề» cho bất kỳ thư nào gửi bởi tiện ích "
"warnquota."

#. Type: string
#. Description
#: ../templates:10001
#, fuzzy
msgid "CC header of warnquota emails:"
msgstr "CC của thư cảnh báo warnquota"

#. Type: string
#. Description
#: ../templates:10001
msgid ""
"The text you specify here is used as the \"CC:\" field of any mail sent by "
"the warnquota utility."
msgstr ""
"Văn bản đưa ra ở đây dùng làm «Chép Cho» (CC:) cho bất kỳ thư nào gửi bởi "
"tiện ích warnquota."

#. Type: string
#. Description
#: ../templates:11001
msgid "Character set in which the e-mail is sent:"
msgstr ""

#. Type: string
#. Description
#: ../templates:11001
#, fuzzy
#| msgid ""
#| "The text you specify here is used as the \"CC:\" field of any mail sent "
#| "by the warnquota utility."
msgid ""
"The text you specify here is used as the \"charset:\" field in the MIME "
"header of any mail sent by the warnquota utility."
msgstr ""
"Văn bản đưa ra ở đây dùng làm «Chép Cho» (CC:) cho bất kỳ thư nào gửi bởi "
"tiện ích warnquota."

#. Type: string
#. Description
#: ../templates:12001
#, fuzzy
msgid "Time slot in which admin gets email:"
msgstr "Khoảng thời gian nhà quản trị nhận thư."

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"During this time slot before the end of the grace period admin will be CCed "
"on all generated emails. Leave empty to get the whole grace period."
msgstr ""
"Trong khoảng thời gian trước khi kết thúc giai đoạn gia hạn nhà quản trị sẽ "
"nhận được Chép Cho (CC) của tất cả những thư tạo ra. Để trống để lấy cả giai "
"đoạn gia hạn."

#. Type: string
#. Description
#: ../templates:13001
#, fuzzy
msgid "Signature of warnquota group emails:"
msgstr "Chữ ký của thư cảnh báo warnquota đến nhóm"

#. Type: string
#. Description
#: ../templates:13001
msgid ""
"The text you specify here is used as signature in any mail sent by the "
"warnquota utility for groups that are over quota. Use \"|\" to specify a "
"line break. Leave empty if you want the default message."
msgstr ""
"Văn bản nhập ở đây được dùng làm chữ ký của thư để tiện ích warnquota gửi "
"tới nhóm người dùng vượt quá giới hạn không gian đĩa. Dùng «|» (ký tự ống "
"dẫn) để chỉ ra một ngắt dòng. Để trống nếu muốn dùng thư mặc định."
